package com.example.icerocktest.modules.notes

import com.example.icerocktest.common.mvp.IListPresenter
import com.example.icerocktest.common.mvp.IListView
import com.example.icerocktest.data.realm.models.RealmMyNote


interface INotesView : IListView<RealmMyNote> {
    fun updateAdapter(noteList: List<RealmMyNote>)
    fun navigateToAddTask(id:Int)
    fun navigateToNote(id:Int)
}

interface INotesPresenter : IListPresenter<RealmMyNote, INotesView> {
    fun onAddClick()
    fun deleteAllNotes()
    fun onNoteClick(note: RealmMyNote)
}