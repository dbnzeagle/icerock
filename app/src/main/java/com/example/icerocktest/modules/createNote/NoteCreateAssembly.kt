package com.example.icerocktest.modules.createNote

import com.example.icerocktest.di.AppComponent
import com.example.icerocktest.di.scopes.ActivityScope
import dagger.Binds
import dagger.Component
import dagger.Module
import dagger.Provides


@ActivityScope
@Component(modules = arrayOf(NoteCreateModule::class), dependencies = arrayOf(AppComponent::class))
interface NoteCreateComponent {
    fun inject(noteCreateActivity: NoteCreateActivity)
}

@Module(includes = arrayOf(NoteCreateModule.Declarations::class))
class NoteCreateModule(val noteCreateView: INoteCreateView) {

    @Provides
    fun provideNoteCreateView(): INoteCreateView {
        return noteCreateView
    }

    @Module
    interface Declarations {
        @Binds
        @ActivityScope
        fun bindNoteCreatePresenter(noteCreatePresenter: NoteCreatePresenter): INoteCreatePresenter
    }

}