package com.example.icerocktest.modules.notes

import rx.lang.kotlin.plusAssign
import com.example.icerocktest.common.mvp.RxPresenter
import com.example.icerocktest.data.realm.models.RealmMyNote
import com.example.icerocktest.repositoies.RealmRepository
import rx.lang.kotlin.toObservable
import javax.inject.Inject

class NotesPresenter @Inject constructor(view: INotesView) : RxPresenter<INotesView>(view),
    INotesPresenter {

    @Inject
    lateinit var realmRepository: RealmRepository

    override fun onAddClick() {
        subscriptions += realmRepository.fetchMyNotesList()
            .flatMap { it.toObservable() }
            .toList()
            .subscribe { note ->
                if(note.isEmpty()){
                    view?.navigateToAddTask( 1)
                }else {
                    note.sortBy { it.id }
                    val data = note.last()
                    view?.navigateToAddTask(data.id + 1)
                }
            }
    }

    override fun onNoteClick(note: RealmMyNote) {
        view?.navigateToNote(note.id)
    }

    override fun onViewAttached() {
        super.onViewAttached()
        fetchData()

    }
    override fun deleteAllNotes() {
        subscriptions+=realmRepository.deleteMyNotesListAll()
            .subscribe {fetchData()}
    }


    private fun fetchData() {
        subscriptions += realmRepository.fetchMyNotesList()
            .flatMap { it.toObservable() }
            .toList()
            .subscribe { note ->
                view?.updateAdapter(note)
            }
    }
}
