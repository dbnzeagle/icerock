package com.example.icerocktest.modules.createNote

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.example.icerocktest.common.mvp.MvpActivity
import com.example.icerocktest.data.realm.models.RealmMyNote
import com.example.icerocktest.di.AppComponent
import kotlinx.android.synthetic.main.activity_create.*
import kotlinx.android.synthetic.main.content_note.*
import org.jetbrains.anko.sdk25.listeners.onClick
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import android.app.Activity
import android.net.Uri
import com.bumptech.glide.Glide
import com.example.icerocktest.R


class NoteCreateActivity : MvpActivity(), INoteCreateView {

    companion object {
        const val NOTE_ID = "id"
        const val EDITABLE = "editable"
    }


    @Inject
    lateinit var presenter: INoteCreatePresenter
    var id = 0
    var checkEdit = false
    var path: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create)
        initViews()
    }

    private fun setDate() {
        val date = SimpleDateFormat("dd.MM.yyyy")
        val currentDate = date.format(Date())
        dateCreated.text = currentDate
    }

    private fun setEditDate() {
        val date = SimpleDateFormat("dd.MM.yyyy")
        val currentDate = date.format(Date())
        editedDate.text = currentDate
    }

    override fun resolveDependencies(appComponent: AppComponent) {
        DaggerNoteCreateComponent.builder()
            .appComponent(appComponent)
            .noteCreateModule(NoteCreateModule(this))
            .build()
            .inject(this)
    }


    override fun beforeDestroy() {
        presenter.dropView()
    }

    override fun close() {
        finish()
    }


    private fun initViews() {
        id = intent.getIntExtra("id", 0)
        checkEdit = intent.getBooleanExtra("editable", false)
        presenter.getNote(id)
        if (!checkEdit) {
            setDate()
        } else setEditDate()
        confirmButton.onClick {
            if (titleCreate.text.toString() != "" && textCreate.text.toString() != "") {
                presenter.onCreateCard(
                    id,
                    titleCreate.text.toString(),
                    textCreate.text.toString(),
                    path,
                    dateCreated.text.toString(),
                    editedDate.text.toString()
                )
            }

        }
    }

    override fun setValues(note: RealmMyNote?) {
        titleCreate.setText(note?.title)
        textCreate.setText(note?.text)
        path = note?.image
        if (checkEdit&&path!=null) {
            Glide.with(this).load(Uri.parse(path)).centerCrop().into(imageView)
        }
        dateCreated.text = note?.dateCreated
        editedDate.text = note?.dateEdited
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(com.example.icerocktest.R.menu.menu_note, menu)
        val icon = menu?.findItem(R.id.actionDeleteNote)
        icon?.isVisible = checkEdit
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.actionDeleteNote) {
            presenter.deleteNote(id)
        } else {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "image/*"
            if (intent.resolveActivity(packageManager) != null) {
                startActivityForResult(intent, 1)
            }
        }
        return super.onOptionsItemSelected(item)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                    path = data?.data.toString()
                Glide.with(this).load(Uri.parse(path)).centerCrop().into(imageView)
            }
        }
    }

}