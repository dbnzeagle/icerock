package com.example.icerocktest.modules.notes

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.example.icerocktest.common.mvp.BaseListActivity
import com.example.icerocktest.di.AppComponent
import javax.inject.Inject
import android.view.Menu
import android.view.MenuItem
import com.example.icerocktest.data.realm.models.RealmMyNote
import com.example.icerocktest.modules.createNote.NoteCreateActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import org.jetbrains.anko.sdk25.listeners.onClick
import org.jetbrains.anko.startActivity
import com.example.icerocktest.R


class NotesActivity : BaseListActivity<RealmMyNote>(), INotesView {


    @Inject
    lateinit var presenter: INotesPresenter

    override fun updateAdapter(noteList: List<RealmMyNote>) {
        var adapter: NotesAdapter? = null
        adapter = NotesAdapter(
            dataList = noteList,
            onItemClick = { recipeModelList -> presenter.onNoteClick(recipeModelList) }
        )
        notes_recycler.adapter = adapter
    }
    override fun navigateToNote(id: Int) {
        startActivity<NoteCreateActivity>(NoteCreateActivity.NOTE_ID to id,
            NoteCreateActivity.EDITABLE to true)
    }

    override fun navigateToAddTask(id:Int) {
        startActivity<NoteCreateActivity>(NoteCreateActivity.NOTE_ID to id)
    }

    override fun resolveDependencies(appComponent: AppComponent) {
        DaggerNotesListComponent.builder()
            .appComponent(appComponent)
            .notesListModule(NotesListModule(this))
            .build()
            .inject(this)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        initViews()
        presenter.onViewAttached()

    }


    private fun initViews() {
        notes_recycler.layoutManager = LinearLayoutManager(this)
        notes_recycler.setHasFixedSize(true)
        confirmButton.onClick {
            presenter.onAddClick()
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.onViewAttached()

        initViews()

    }

    override fun beforeDestroy() {
        presenter.dropView()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.actionDelete){
            presenter.deleteAllNotes()
        }
        return super.onOptionsItemSelected(item)

    }
}
