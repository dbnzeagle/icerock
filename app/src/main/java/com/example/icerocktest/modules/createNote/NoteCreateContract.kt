package com.example.icerocktest.modules.createNote

import com.example.icerocktest.common.mvp.IPresenter
import com.example.icerocktest.common.mvp.IView
import com.example.icerocktest.data.realm.models.RealmMyNote

interface INoteCreateView : IView {
    fun close()
    fun setValues(note:RealmMyNote?)
}

interface INoteCreatePresenter : IPresenter<INoteCreateView> {
    fun getNote(id:Int)
    fun deleteNote(id:Int)
    fun onCreateCard(
        id:Int,
        title: String?,
        text: String?,
        image: String?,
        dateCreated: String?,
        dateEdited: String?
    )
}