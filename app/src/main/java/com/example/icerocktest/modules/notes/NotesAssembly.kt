package com.example.icerocktest.modules.notes

import dagger.Binds
import dagger.Component
import dagger.Module
import dagger.Provides
import com.example.icerocktest.di.AppComponent
import com.example.icerocktest.di.scopes.ActivityScope


@ActivityScope
@Component(modules = arrayOf(NotesListModule::class), dependencies = arrayOf(AppComponent::class))
interface NotesListComponent {
    fun inject(notesActivity: NotesActivity)
}


@Module(includes = arrayOf(NotesListModule.Declarations::class))
class NotesListModule(val notesListView: INotesView) {

    @Provides
    fun provideNotesView(): INotesView {
        return notesListView
    }

    @Module
    interface Declarations {
        @Binds
        @ActivityScope
        fun bindNotesListPresenter(notesListPresenter: NotesPresenter): INotesPresenter
    }
}
