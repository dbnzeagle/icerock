package com.example.icerocktest.modules.createNote


import com.example.icerocktest.common.mvp.RxPresenter
import com.example.icerocktest.repositoies.RealmRepository
import com.example.icerocktest.models.Note
import rx.lang.kotlin.plusAssign
import javax.inject.Inject


class NoteCreatePresenter @Inject constructor(view: INoteCreateView) : RxPresenter<INoteCreateView>(view),
    INoteCreatePresenter {

    @Inject lateinit var realmRepository: RealmRepository


    override fun onViewDetached() {
        realmRepository.close()
        super.onViewDetached()
    }
    override fun getNote(id:Int){
        subscriptions += realmRepository.fetchMyNote(id)
            .subscribe { note ->
                view?.setValues(note)
            }
    }

    override fun deleteNote(id: Int) {
        subscriptions += realmRepository.deleteMyNote(id)
            .subscribe()
            view?.close()
    }

    override fun onCreateCard(id:Int,title: String?,text:String? ,image: String?, dateCreated: String?,dateEdited: String?) {
            val data=Note(
                id = id,
                title = title,
                text = text,
                image = image,
                dateCreated = dateCreated,
                dateEdited = dateEdited
            )
            subscriptions+=realmRepository.saveMyNote(data)
                .subscribe({view?.close()},{})
    }

}
