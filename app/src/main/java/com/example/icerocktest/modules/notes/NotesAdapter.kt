package com.example.icerocktest.modules.notes

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.icerocktest.R
import com.example.icerocktest.data.realm.models.RealmMyNote
import kotlinx.android.synthetic.main.recycler_view_item.view.*
import org.jetbrains.anko.sdk25.listeners.onClick


class NotesAdapter(
    val dataList:
    List<RealmMyNote>,
    val onItemClick: ((RealmMyNote) -> Unit)? = null
) :
    RecyclerView.Adapter<NotesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recycler_view_item
            , parent, false)
        return ViewHolder(view, onItemClick)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataList[position])

    }


    override fun getItemCount(): Int {
        return dataList.size
    }

    class ViewHolder(itemView: View, val onItemClick: ((RealmMyNote) -> Unit)? = null) :
        RecyclerView.ViewHolder(itemView) {
        fun bind(note: RealmMyNote) {
            itemView.apply {
                title.text = note.title
                date_created.text = note.dateCreated
                note_card.onClick { onItemClick?.invoke(note) }

            }
        }

    }
}
