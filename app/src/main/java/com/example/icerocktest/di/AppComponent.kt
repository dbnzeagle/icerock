package com.example.icerocktest.di

import android.app.Application
import android.content.Context
import dagger.Component
import com.example.icerocktest.di.modules.AppModule
import com.example.icerocktest.modules.createNote.NoteCreateActivity
import com.example.icerocktest.modules.notes.NotesActivity
import io.realm.Realm

import javax.inject.Singleton


@Singleton
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {
    fun provideApplication(): Application
    fun provideContext(): Context
    fun provideNotesActivity(): NotesActivity
    fun provideNoteCreateActivity(): NoteCreateActivity
    fun provideRealm(): Realm

}