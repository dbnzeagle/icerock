package com.example.icerocktest.di.modules

import android.app.Application
import android.content.Context
import com.example.icerocktest.IceRockApplication
import com.example.icerocktest.modules.createNote.NoteCreateActivity
import com.example.icerocktest.modules.notes.NotesActivity
import dagger.Module
import dagger.Provides
import io.realm.Realm
import io.realm.RealmConfiguration
import javax.inject.Singleton

@Module
class AppModule(val application: IceRockApplication) {

    @Provides
    fun provideApplication():Application{
        return application
    }
    @Provides
    fun provideRealm(): Realm {
        val realmConfiguration= RealmConfiguration.Builder().schemaVersion(9L).deleteRealmIfMigrationNeeded().build()
        return Realm.getInstance(realmConfiguration)
    }
    @Provides
    @Singleton
    fun provideContext():Context{
        return application.applicationContext
    }
    @Provides
    fun provideNotesActivity(): NotesActivity {
        return NotesActivity()
    }
    @Provides
    fun provideNoteCreateActivity(): NoteCreateActivity {
        return NoteCreateActivity()
    }



}