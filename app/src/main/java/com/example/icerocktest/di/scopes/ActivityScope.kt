package com.example.icerocktest.di.scopes

import javax.inject.Scope

@Scope
annotation class ActivityScope