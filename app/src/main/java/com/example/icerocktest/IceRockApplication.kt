package com.example.icerocktest

import android.app.Application
import android.support.v7.app.AppCompatDelegate
import com.example.icerocktest.di.AppComponent
import com.example.icerocktest.di.DaggerAppComponent
import com.example.icerocktest.di.modules.AppModule
import io.realm.Realm


class IceRockApplication : Application() {

    lateinit var appComponent: AppComponent
        private set

    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        appComponent = DaggerAppComponent.builder().appModule(AppModule(this)).build()


    }

}