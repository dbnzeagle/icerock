package com.example.icerocktest.repositoies

import com.example.icerocktest.common.mvp.Repository
import com.example.icerocktest.data.realm.models.RealmMyNote
import com.example.icerocktest.models.Note
import io.realm.Realm
import rx.Observable
import javax.inject.Inject

class RealmRepository @Inject constructor(private val realm: Realm) : Repository() {

    fun close() {
        realm.close()
    }

    fun fetchMyNotesList(): Observable<List<RealmMyNote>> {
        return Observable.just(
            realm
                .where(RealmMyNote::class.java)
                .findAll()
        )
    }

    fun fetchMyNote(id:Int): Observable<RealmMyNote> {
        return Observable.just(
            realm
                .where(RealmMyNote::class.java)
                .equalTo("id",id)
                .findFirst()
        )

    }

    fun saveMyNote(note: Note): Observable<Boolean> {
        return fetchMyNote(note.id)
            .concatMap {

                val realmNote= RealmMyNote(
                    id = note.id,
                    title = note.title,
                    text = note.text,
                    image = note.image,
                    dateCreated = note.dateCreated,
                    dateEdited = note.dateEdited
                )
                Observable.create<Boolean> { sub ->
                    realm.executeTransactionAsync(
                        {
                            it.copyToRealmOrUpdate(realmNote)
                        },
                        {
                            sub.onNext(true)
                        },
                        { error ->
                            sub.onError(error)
                        }
                    )
                }
            }
    }
    fun deleteMyNote(id: Int):Observable<Boolean>{
        return Observable.create<Boolean>{
            realm.executeTransactionAsync{
                it.where(RealmMyNote::class.java)
                    .equalTo("id",id)
                    .findFirst().deleteFromRealm()
            }
        }
    }
    fun deleteMyNotesListAll(): Observable<Boolean> {
        return Observable.create<Boolean> { sub ->
            realm.executeTransactionAsync(
                {
                    it.where(RealmMyNote::class.java)
                        .findAll()
                        .deleteAllFromRealm()
                },
                {
                    sub.onNext(true)
                },
                { error ->
                    sub.onError(error)

                }
            )
        }
    }
}
