package com.example.icerocktest.data.realm.models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class RealmMyNote(
        @PrimaryKey
        open var id: Int=0,
        open var title: String? = "",
        open var text:String?="",
        open var image: String?="",
        open var dateCreated: String? = "",
        open var dateEdited: String? = ""
        ):RealmObject()
