package com.example.icerocktest.common.mvp


abstract class BasePresenter<V: IView>(view: V) : IPresenter<V> {
    protected var view:V?= view

    override fun takeView(view: V){
        this.view=view
    }

    override fun onViewAttached(){

    }
    override fun setFilter(p:String){

    }

    override fun dropView(){
        view=null
        onViewAttached()

    }

    override fun onViewDetached(){}

}