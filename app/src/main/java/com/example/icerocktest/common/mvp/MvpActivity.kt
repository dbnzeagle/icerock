package com.example.icerocktest.common.mvp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.icerocktest.IceRockApplication
import com.example.icerocktest.di.AppComponent


abstract class MvpActivity : AppCompatActivity(), IView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        resolveDependencies(application().appComponent)

    }

    override fun onDestroy() {
        beforeDestroy()
        super.onDestroy()
    }

    abstract fun resolveDependencies(appComponent: AppComponent)

    abstract fun beforeDestroy()

    override fun application(): IceRockApplication {
        return application as IceRockApplication
    }

}