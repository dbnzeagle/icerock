package com.example.icerocktest.common.mvp

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import com.example.icerocktest.IceRockApplication


abstract class BaseListActivity<M> : MvpActivity(), IListView<M> {
    private var dataset: MutableList<M> = ArrayList()
    private var adapter: RecyclerView.Adapter<*>?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        resolveDependencies(application().appComponent)
    }
    override fun showData(dataset: List<M>) {
        this.dataset.clear()
        this.dataset.addAll(dataset)
        adapter?.notifyDataSetChanged()
    }


    override fun notifyItemRemoved(item: M) {
        val index = dataset.indexOf(item)
        dataset.removeAt(index)
        adapter?.notifyItemRemoved(index)
    }


    override fun onDestroy() {
        beforeDestroy()
        super.onDestroy()
    }


    override fun application(): IceRockApplication {
        return application as IceRockApplication
    }



}
