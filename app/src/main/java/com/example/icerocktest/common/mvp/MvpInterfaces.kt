package com.example.icerocktest.common.mvp

import com.example.icerocktest.IceRockApplication


interface IView {
    fun application(): IceRockApplication
}

interface IListView<M> : IView {
    fun showData(dataset: List<M>)
    fun notifyItemRemoved(item: M)
}

interface IPresenter<in V : IView> {
    fun setFilter(p:String)
    fun takeView(view: V)
    fun onViewAttached()
    fun dropView()
    fun onViewDetached()
}

interface IListPresenter<M, V : IListView<*>> : IPresenter<V> {
}

