package com.example.icerocktest.models



data class Note(

    var id: Int,

    var title: String?,

    var text:String?,

    var image: String?,

    var dateCreated: String?,

    var dateEdited: String?
)
